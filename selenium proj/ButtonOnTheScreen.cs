﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using OpenQA.Selenium;
using OpenQA.Selenium.Interactions;
using OpenQA.Selenium.Support.UI;
using System.Threading;
using System.Collections;
using System.Collections.Generic;

namespace selenium_proj
{
    [TestClass]
    class ButtonOnTheScreen
    {
        IWebDriver Driver;
        public ButtonOnTheScreen(IWebDriver driver)
        {
            this.Driver = driver;
        }

     [TestMethod]
     public void click (string field)
        {
                      
            switch (field.ToUpper())
            {
                case"HOME":case "FRAMES AND WINDOWS":
                case "REGISTRATION":
                case "ALERT":
               
                    IWebElement buttons = Driver.FindElement(By.XPath("//div[@class='container main-nav']//li/a[text()='"+field+"']"));
                    buttons.Click();
                    Driver.SwitchTo().ParentFrame();
                    break;
                case "ANIMATIONS":
                case "DEFAULT FUNCTIONALITY":
                case "FORMAT DATE":
                case "DISPLAY MONTH & YEAR":
                    string lower = field.ToLower();
                    var newField = char.ToUpper(lower[0]) + lower.Substring(1);
                    IWebElement button = Driver.FindElement(By.XPath("//div[@class='internal_navi']//a[text()='"+newField+ "']"));
                    button.Click();
                    Driver.SwitchTo().ParentFrame();
                    break;
                

            }
            
      }

        [TestMethod]
        public void click(string field, string dropdownvalue)
        {
            switch (field.ToUpper())
            {
                case "INTERACTION":
                case "DYNAMIC ELEMENTS":
                case "WIDGET":
                    IWebElement dropdownButton = Driver.FindElement(By.XPath("//div[@class='container main-nav']//li/a[text()='" + field + "']"));
                    Actions actions = new Actions(Driver);
                    actions.MoveToElement(dropdownButton).Click().Perform();             

                    switch (dropdownvalue.ToUpper())
                    {

                        case "DRAGGABLE":
                        case "DROPPABLE":
                        case "RESIZEABLE":
                        case "SELECTABLE":
                        case "AUTOCOMPLETE":
                        case "ACCORDION":
                        case "MENU":
                        case "DATEPICKER":
                        case "SLIDER":
                        case "SUBMIT BUTTON CLICKED":
                        case "DROPDOWN":
                        case "TOOLTIP":

                            IWebElement dropdownbuttons = Driver.FindElement(By.XPath("//div[@class='container main-nav']//ul[@class='dropdown']/li/a[text()='"+dropdownvalue+"']"));
                            dropdownbuttons.Click();                           
                            Driver.SwitchTo().DefaultContent();
                            break;
                        default:

                            System.Console.WriteLine("Something went wrong");
                            break;

                    }
                    break;
                
                default:
                case "ANIMATIONS":
                    IWebElement iframe = Driver.FindElement(By.XPath("//*[@id = 'example-1-tab-2']//iframe[@class='demo-frame']"));
                    Driver.SwitchTo().Frame(iframe);
                    IWebElement dropDown = Driver.FindElement(By.Id("anim"));
                    dropDown.Click();
                   
                    
                    switch (dropdownvalue.ToUpper())
                    {
                        default:
                            

                            SelectElement dropdownValue = new SelectElement(dropDown);
                             dropdownValue.SelectByText(dropdownvalue);
                            
                            Driver.SwitchTo().DefaultContent();
                            break;

                    }
                    Driver.SwitchTo().DefaultContent();
                    break;

               


            }

        }
      
        [TestMethod]
        public void signIN(string username, string password)
        {
            IWebElement signin = Driver.FindElement(By.XPath("//div[@class='span_3_of_4']//a[@class='fancybox' and text()='Signin']"));
            signin.Click();
            IWebElement Username = Driver.FindElement(By.XPath("//div[@id='login']//input[@type='text' and @name='username']"));
            Username.SendKeys(username);
            IWebElement Password = Driver.FindElement(By.XPath("//div[@id='login']//input[@name='password']"));
            Password.SendKeys(password);
            IWebElement submitButton = Driver.FindElement(By.XPath("//div[@id='login']//input[@class='button' and @value='Submit']"));
            submitButton.Click();

        }

        [TestMethod]
        public void verify(string field, string expectedTooltip)
        {
            switch (field.ToUpper())
            {
                case "YOUR AGE:":
                   IWebElement iframe = Driver.FindElement(By.XPath("//*[@id = 'example-1-tab-1']//iframe"));
                   Driver.SwitchTo().Frame(iframe);
                   Thread.Sleep(300);
                    IWebElement Age = Driver.FindElement(By.XPath("//p[4]//*[@id='age']"));
                    Actions action = new Actions(Driver);
                    action.MoveToElement(Age).Build().Perform();

                    string actualTootltip = Age.GetAttribute("title");
                    if(actualTootltip.Equals(expectedTooltip))
                    {
                        System.Console.WriteLine("Success the value is: ", actualTootltip);
                    }
                    else
                    {
                        System.Console.WriteLine("Text not matched");

                    }
                    break;

            }
            Driver.SwitchTo().DefaultContent();

        }

        public void set(string field, string value)
        {
            switch (field.ToUpper())
            {
                case "DATE":

                    IWebElement Iframe = Driver.FindElement(By.XPath("//div[@id='example-1-tab-2']//iframe[@src='datepicker/defult2.html']"));
                    Driver.SwitchTo().Frame(Iframe);
                    IWebElement date = Driver.FindElement(By.XPath("//input[@type='text' and @id='datepicker' and @class='hasDatepicker']"));
                    Actions act = new Actions(Driver);
                    act.MoveToElement(date).Build().Perform();
                    date.SendKeys(value);//value should be like mmddyy example 11/16/2020 (16 nov 2020)
                    date.SendKeys(Keys.Tab);
                    Thread.Sleep(100);
                    Driver.SwitchTo().ParentFrame();
                    break;
            }



        }
       
            

    }
}
