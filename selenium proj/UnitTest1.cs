﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium;
using System.Threading;
using OpenQA.Selenium.Interactions;




namespace selenium_proj
{
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        public void TestMethod1()
        {
            //create the object for Iwebdriver
            IWebDriver chromeDriver = new ChromeDriver();
            // Create the object of class ButtonOnTheScreen
            ButtonOnTheScreen form = new ButtonOnTheScreen(chromeDriver);
            //Passing the URL
            chromeDriver.Url = "https://way2automation.com/way2auto_jquery/index.php";
            chromeDriver.Manage().Window.Maximize();
            //Begin
            form.signIN("Divyanshu", "Divyanshu123"); 
            Thread.Sleep(5000);
            //Click on the header dropdown
            form.click("Widget", "Tooltip");
            //verify text
            form.verify("Your age:", "We ask for your age only for statistical purposes.");
            //Click on the value from dropdown and select date
            form.click("Widget", "Datepicker");
            form.click("ANIMATIONS");
            form.click("Animations", "Bounce (UI Effect)");
            form.set("date", "11/16/2020"); 
            //Move the slidder
            form.click("Widget", "Slider");
            chromeDriver.SwitchTo().DefaultContent();
            IWebElement iframe = chromeDriver.FindElement(By.XPath("//iframe[@class='demo-frame']"));
            chromeDriver.SwitchTo().Frame(iframe);
            IWebElement slidder = chromeDriver.FindElement(By.XPath("//div[@class='ui-slider-range ui-widget-header ui-corner-all ui-slider-range-max']"));
            Actions act = new Actions(chromeDriver);
            act.ClickAndHold(slidder).MoveByOffset(2,6)
           .Release().Build().Perform();

            
           




































        }






    }
}
