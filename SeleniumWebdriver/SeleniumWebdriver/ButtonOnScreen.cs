﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Interactions;
using System.IO;
using OpenQA.Selenium.Support.UI;
using System.Collections.Generic;
using System.Linq;

namespace SeleniumWebdriver
{
    [TestClass]
    public class ButtonOnScreen
    {

        IWebDriver driver;
        public ButtonOnScreen(IWebDriver driver)
        {
            this.driver = driver;

         }

        [TestMethod]
        public void click(string field, string Dropdown)
        {
            switch (field.ToUpper())
            {
                case "WIDGET":
                    IWebElement SelectOptions = driver.FindElement(By.XPath("//nav[@id='main-nav']//li/a[text()='" + field + "']"));
                    Actions actions = new Actions(driver);
                    actions.MoveToElement(SelectOptions).Click().Perform();


                    switch (Dropdown.ToUpper())
                    {

                        case "AUTOCOMPLETE":
                        case "ACCORDION":
                        case "MENU":
                        case "DATEPICKER":
                        case "SLIDER":

                            IWebElement SelectWidgetOptions = driver.FindElement(By.XPath("//div[@class='container main-nav']//ul[@class='dropdown']/li/a[text()='" + Dropdown + "']"));
                            SelectWidgetOptions.Click();
                            break;

                        default:
                            Console.WriteLine("Invalid Selection");
                            break;
                    }

                    break;
            }

        }

        [TestMethod]
        public void click(string field, string subElement, string selection)
        {
            switch (field.ToUpper())
            {
                case "MENU WITH SUB MENU":
         
                    IWebElement SelectmenuOptions = driver.FindElement(By.XPath("//*[@href='#example-1-tab-2']"));
                    SelectmenuOptions.Click();

                    IWebElement iframe = driver.FindElement(By.XPath("//*[@id='example-1-tab-2']//iframe"));
                    driver.SwitchTo().Frame(iframe);

                    switch (subElement.ToUpper())
                    {
                        case "DELPHI":
                            
                            IWebElement SelectSubMenuOptions = driver.FindElement(By.Id("ui-id-8"));
                            Actions actions = new Actions(driver);
                            actions.MoveToElement(SelectSubMenuOptions).Click().Perform();
                            //driver.SwitchTo().ParentFrame();
                            

                            switch (selection.ToUpper())
                            {

                                case "SUB MENU 3":
                                IWebElement delphiSelections = driver.FindElement(By.Id("ui-id-11"));
                                   // Actions Action = new Actions(driver);
                                    actions.MoveToElement(delphiSelections).Click().Perform();
                                    driver.SwitchTo().DefaultContent();
                                    break;
                                default:
                                    Console.WriteLine("Wrong Selection");
                                    break;
                            }

                            break;
                    }
                    break;
                default:
                    break;
            }



        }

             [TestMethod]
            public void click(string field)
            {

            switch (field.ToUpper())
            {
                case "REGISTRATION":
                    IWebElement clickRegistration = driver.FindElement(By.XPath("//nav[@id='main-nav']//li/a[text()='" + field + "']"));
                    clickRegistration.Click();
                    break;

                case "SUBMIT":

                    IWebElement clickSubmit = driver.FindElement(By.XPath("//fieldset//input[@type='submit']"));
                    clickSubmit.Click();
                    break;

                default:
                    Console.WriteLine("Wrong Choice");
                    break;
            }
            

            }

        [TestMethod]
        public void set(string fieldName, string Value)
        {

            switch (fieldName.ToUpper())
            {
                case "FIRST NAME:":

                 IWebElement FirstName = driver.FindElement(By.XPath("//div[@class='registration_form']//input[@name='name']"));
                 FirstName.SendKeys(Value);

                    break;

                case "LAST NAME:":

                    IWebElement lastName = driver.FindElement(By.XPath("//div[@class='registration_form']//p[2]//input[@type='text']"));
                    lastName.SendKeys(Value);

                    break;

                case "MARITAL STATUS:":
                    switch (Value.ToUpper())
                    {
                        case "SINGLE":

                            IWebElement single = driver.FindElement(By.XPath("//div[@class='radio_wrap']//label[1]//input[@type='radio']"));
                            single.Click();

                            break;

                        case "MARRIED":
                            IWebElement married = driver.FindElement(By.XPath("//div[@class='radio_wrap']//label[2]//input[@type='radio']"));
                            married.Click();

                            break;

                        case "DIVORCED":
                            IWebElement divorced = driver.FindElement(By.XPath("//div[@class='radio_wrap']//label[3]//input[@type='radio']"));
                            divorced.Click();

                            break;

                        default:
                            Console.WriteLine("Wrong selection");

                            break;
                    }

                     break;

                case "COUNTRY:":
                    IWebElement CountrySelect = driver.FindElement(By.XPath("//form[@class='form_box']//select"));
                    SelectElement select = new SelectElement(CountrySelect);
                    select.SelectByText("India");
                   
                    break;

                case "DATE OF BIRTH:":
                    
                            IWebElement month = driver.FindElement(By.XPath("//form[@class='form_box']//div/select"));
                    
                            SelectElement selectValue1 = new SelectElement(month);
                            selectValue1.SelectByText("1");
                            


                            IWebElement day = driver.FindElement(By.XPath("//form[@class='form_box']//div[2]/select"));
                    
                            SelectElement selectValue2 = new SelectElement(day);
                            selectValue2.SelectByText("1");
                           

                            IWebElement year = driver.FindElement(By.XPath("//form[@class='form_box']//div[3]/select"));
                    
                            SelectElement selectValue3 = new SelectElement(year);
                            selectValue3.SelectByText("2014");
                            
                 
                    break;


                case "HOBBY:":
                    switch (Value.ToUpper())
                    {
                        case "DANCE":
                            IWebElement dance = driver.FindElement(By.XPath("//div[@class='radio_wrap']/label[1]/input[@type='checkbox']"));
                            Actions actions = new Actions(driver);
                            actions.MoveToElement(dance).Click().Perform();
                            break;

                        case "READING":
                            IWebElement reading = driver.FindElement(By.XPath("//div[@class='radio_wrap']/label[2]/input[@type='checkbox']"));
                            Actions actionsreading = new Actions(driver);
                            actionsreading.MoveToElement(reading).Click().Perform();
                            break;

                        case "CRICKET":
                            IWebElement cricket = driver.FindElement(By.XPath("//div[@class='radio_wrap']/label[3]/input[@type='checkbox']"));
                            Actions actionsCricket = new Actions(driver);
                            actionsCricket.MoveToElement(cricket).Click().Perform();
                            break;

                        default:
                            Console.WriteLine("Wrong Choice");
                            break;
                    }

                    break;


                case "PHONE NUMBER:":
                    IWebElement pNumber = driver.FindElement(By.XPath("//fieldset[@class='fieldset']//input[@name='phone']"));
                    pNumber.SendKeys(Value);

                    break;

                case "USERNAME:":
                    IWebElement username = driver.FindElement(By.XPath("//fieldset[@class='fieldset']//input[@name='username']"));
                    username.SendKeys(Value);

                    break;

                case "E-MAIL:":
                    IWebElement email = driver.FindElement(By.XPath("//fieldset[@class='fieldset']//input[@name='email']"));
                    email.SendKeys(Value);

                    break;

                case "ABOUT YOURSELF:":
                    IWebElement aboutYourself = driver.FindElement(By.XPath("//fieldset//textarea"));
                    aboutYourself.SendKeys(Value);
                    break;

                case "PASSWORD:":
                    IWebElement password = driver.FindElement(By.XPath("//fieldset[@class='fieldset']//input[@name='password']"));
                    password.SendKeys(Value);

                    break;

                case "CONFIRM PASSWORD:":
                    IWebElement cpassword = driver.FindElement(By.XPath("//fieldset[@class='fieldset']//input[@name='c_password']"));
                    cpassword.SendKeys(Value);

                    break;




                default:
                    Console.WriteLine("Wrong Choice");
                    break;
        }

              
       
             }



            [TestMethod]
            public void signIN(string username, string password)
            {
                IWebElement signin = driver.FindElement(By.XPath("//div[@class='span_3_of_4']//a[@class='fancybox' and text()='Signin']"));
                signin.Click();
                IWebElement Username = driver.FindElement(By.XPath("//div[@id='login']//input[@type='text' and @name='username']"));
                Username.SendKeys(username);
                IWebElement Password = driver.FindElement(By.XPath("//div[@id='login']//input[@name='password']"));
                Password.SendKeys(password);
                IWebElement submitButton = driver.FindElement(By.XPath("//div[@id='login']//input[@class='button' and @value='Submit']"));
                submitButton.Click();



            }

        
    }
}
