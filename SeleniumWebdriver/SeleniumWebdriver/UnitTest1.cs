﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using System.Threading;
using System.IO;

namespace SeleniumWebdriver
{
    [TestClass]
    public class UnitTest1
    {
        public object Driver { get; private set; }

        [TestMethod]
        public void TestMethod1()
        {
            //create the object for Iwebdriver
            IWebDriver CDriver = new ChromeDriver();
            // Create the object of class ButtonOnScreen
            ButtonOnScreen buttons = new ButtonOnScreen(CDriver);
            //Passing the URL
            CDriver.Url = "https://way2automation.com/way2auto_jquery/index.php";
            CDriver.Manage().Window.Maximize();
            //Begin
            buttons.signIN("monica26", "Monica@117");
            Thread.Sleep(4000);
            // Click on the header dropdown
            buttons.click("Widget", "Menu");
            // click on the menu with sub menu
            buttons.click("Menu With Sub Menu","Delphi","Sub Menu 3");
            //click on Registration form and fill the details
            buttons.click("Registration");
            buttons.set("First Name:", "Monica");
            buttons.set("Last Name:", "Dabas");
            buttons.set("Marital Status:", "Single");
            buttons.set("Hobby:","Dance");
            buttons.set("Hobby:", "Reading");
            buttons.set("Country:", "India");
            buttons.set("Date of Birth:", "1");
            buttons.set("Date of Birth:", "1");
            buttons.set("Date of Birth:", "2014");
            buttons.set("Phone Number:", "8700448597");
            buttons.set("username:", "mdabas");
            buttons.set("e-mail:", "monica.dabas@gmail.com");
            buttons.set("about yourself:", "Hi! I am Automation Tester");
            buttons.set("password:", "Monica@26");
            buttons.set("confirm password:", "Monica@26");
            

            string Title = CDriver.Url;
            buttons.click("Submit");
            string currentTitle = CDriver.Url;

            Assert.AreNotEqual(Title, currentTitle);

            




        }

    
    }
}
